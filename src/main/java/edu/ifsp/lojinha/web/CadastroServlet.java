package edu.ifsp.lojinha.web;

import java.io.IOException;

import edu.ifsp.lojinha.modelo.Cliente;
import edu.ifsp.lojinha.persistencia.ClienteDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CadastroServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		
		Cliente cliente = new Cliente(nome, email);
		ClienteDAO dao = new ClienteDAO();
		dao.save(cliente);
		
		request.setAttribute("cliente", cliente);
		
		RequestDispatcher rd = request.getRequestDispatcher("cadastro.jsp");
		rd.forward(request, response);		
	}
}
