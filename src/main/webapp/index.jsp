<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
<meta charset="UTF-8">
<title>Lojinha da Julia</title>
</head>

<body>
	<h1>Lojinha do Julia</h1>
	
	<c:choose>
		<c:when test="${not empty sessionScope.usuario}">
			<p> Olá, ${sessionScope.usuario.name} ! (<a href="logout">Sair</a>)</p>
		</c:when>
		
		<c:otherwise>
			<p> <a href="login"> Login </a><p/>
		</c:otherwise>
	</c:choose>
	
	<p>Faça <a href="cadastro.html">aqui</a> o seu cadastro de cliente!
	
	<c:if test="${not empty sessionScope.usuario }">
		<p><a href="listar.sec">Listar clientes</a></p>	
	</c:if>
</body>
</html>